import ServiceManager from '@/packages/developerAssistant/ServiceManager'
import DeveloperConsole from '@/packages/developerAssistant/DeveloperConsole'
import DataMutations from '@/helpers/system/DataMutations'
import * as providers from '@/settings/services'


const console = new DeveloperConsole({
  serviceManager: new ServiceManager(DataMutations, providers)
})

export {
  console as DeveloperConsole
}
