import SetsHelper from '@/helpers/evaluation/findItem'
import Finder from '@/packages/Finder'

const fabric = (containerOptions) => {
  return new Finder({SetsHelper}, containerOptions)
}

export {
  fabric as Finder
}
