import SetsHelper from '@/helpers/evaluation/intersectionOfSets'
import Container from '@/packages/Container'

const fabric = (containerOptions) => {
  return new Container({SetsHelper}, containerOptions)
}

export {
  fabric as Container
}
