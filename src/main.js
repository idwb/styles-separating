// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import filters from './filters'
import {glob} from './mixins'

Vue.config.productionTip = false


new Vue({
  el: '#app',
  mixins: glob,
  router,
  components: { App },
  template: '<App/>'
})
/* eslint-disable no-new */
