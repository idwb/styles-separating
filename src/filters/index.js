import Vue from 'vue'

Vue.filter('hello', function (value) {
  return 'Hello, ' + value.toString().toUpperCase() + '!'
})
