import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import YandexRaion from '@/components/YandexRaion'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/yandex-raion',
      name: 'YandexRaion',
      component: YandexRaion
    },
  ]
})
