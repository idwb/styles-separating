export default {

  objectToArray(object, keyName = "key") {

    let array = []

    for (let key in object) {
      let value = object[key]
      value[keyName] = key
      array.push(value)
    }

    return array

  }
  
}
