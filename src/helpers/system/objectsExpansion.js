export default {

  attachMethod (parent, methodName, method) {
    parent[methodName] = function (parent, ...args) {
      method(parent, ...args)
    }
  }
  
}
