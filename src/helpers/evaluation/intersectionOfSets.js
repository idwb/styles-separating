export default {

  howManyFit (containerSize, contentSize) {
    return Math.floor(containerSize / contentSize)
  }

}
