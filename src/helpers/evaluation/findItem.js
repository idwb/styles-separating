export default {

  findItem (array, whatFind) {
    if(array === undefined){
      console.log('helpers/evaluation/findItem: Не передан масив')
    }else{
      for(var i = 0; i < array.length; i++){
        if(array[i] === whatFind){
          return i
        }
      }
      return false
    }
  }

}
