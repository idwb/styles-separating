export default class {

  constructor ({SetsHelper}, options) {
    this.sets = SetsHelper
    this.array = options
  }

  findItem (item) {
    return this.sets.findItem(this.array, item)
  }

}
