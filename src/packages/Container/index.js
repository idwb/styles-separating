export default class {

  constructor ({SetsHelper}, options) {
    this.sets = SetsHelper
    this.width = options.width
  }

  howManyFit (childSize) {
    return this.sets.howManyFit(this.width, childSize)
  }
  
}
