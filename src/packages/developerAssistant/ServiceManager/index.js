export default class {

  constructor({objectToArray}, providers) {
    this.providers = objectToArray(providers, "serviceName");
  }

  getIncluded () {
    return this.providers
  }

}
