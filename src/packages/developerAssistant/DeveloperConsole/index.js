export default class {
  constructor ({serviceManager}) {
    this.serviceManager = serviceManager
  }

  logServices () {
    let message = "Подключенные сервисы: \n"
    let services = this.serviceManager.getIncluded()

    services.forEach(i => {
      message += i.serviceName + "\n"
    })

    console.log(message)
  }
}
